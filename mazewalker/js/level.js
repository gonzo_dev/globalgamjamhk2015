
var game = new Phaser.Game(500, 500, Phaser.AUTO, 'phaser-example', { preload: preload, create: create });

//global variables

//var myId=0;

//var land;

//var shadow;
var tank;
//var turret;
//var player;
//var tanksList;
//v//ar explosions;

//var logo;
var player00;

//var cursors;

//var bullets;
//var fireRate = 100;
//var nextFire = 0;

var currentSpeed = 0;
var player00;
var ready = false;
//var eurecaServer;

Player = function (index, game, player, bullets) 
{

    var x = 0;//game.world.randomX;
    var y = 0://game.world.randomY;
	
	
    this.tank = game.add.sprite(x, y, 'enemy', 'tank1');
    this.tank.anchor.set(0.5);
    game.physics.enable(this.tank, Phaser.Physics.ARCADE);
    this.tank.body.immovable = false;
    this.tank.body.collideWorldBounds = true;
    this.tank.body.bounce.setTo(1, 1);

    game.physics.arcade.velocityFromRotation(this.tank.rotation, 100, this.tank.body.velocity);
	
}



function preload ()
 {

    game.load.atlas('tank', 'assets/tanks.png', 'assets/tanks.json');
    game.load.atlas('enemy', 'assets/enemy-tanks.png', 'assets/tanks.json');
   // game.load.image('logo', 'assets/logo.png');
   // game.load.image('bullet', 'assets/bullet.png');
   // game.load.image('earth', 'assets/scorched_earth.png');
 //   game.load.spritesheet('kaboom', 'assets/explosion.png', 64, 64, 23);
 //   game.load.spritesheet('wall', 'assets/Maze_Walkers_Tiles.png', 10, 10, 5);
    game.load.tilemap('mazelevel', 'assets/level_01.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.image('tiles', 'assets/Maze_Walkers_Tiles2.png');
}


var map;
var layer;

function create () {

    //  Resize our game world to be a 2000 x 2000 square
    game.world.setBounds(-1000, -1000, 2000, 2000);
	//game.stage.disableVisibilityChange  = true;
	
    //  Our tiled scrolling background
    //land = game.add.tileSprite(0, 0, 800, 600, 'earth');
    //land.fixedToCamera = true;
    //  The base of our tank
    tank = game.add.sprite(0, 0, 'tank', 'tank1');
    tank.anchor.setTo(0.5, 0.5);
    tank.animations.add('move', ['tank1', 'tank2', 'tank3', 'tank4', 'tank5', 'tank6'], 20, true);

    //  This will force it to decelerate and limit its speed
    game.physics.enable(tank, Phaser.Physics.ARCADE);
    tank.body.drag.set(0.2);
    tank.body.maxVelocity.setTo(400, 400);
    tank.body.collideWorldBounds = true;

	player00 = new Player(i, game, tank, enemyBullets);
	
    //tanksList = {}
	game.stage.backgroundColor = '#797878';

    //  The 'mario' key here is the Loader key given in game.load.tilemap
    map = game.add.tilemap('mazelevel');

    //  The first parameter is the tileset name, as specified in the Tiled map editor (and in the tilemap json file)
    //  The second parameter maps this name to the Phaser.Cache key 'tiles'
    map.addTilesetImage('MazeLevel1', 'tiles');
    
    //  Creates a layer from the World1 layer in the map data.
    //  A Layer is effectively like a Phaser.Sprite, so is added to the display list.
    layer = map.createLayer('World1');

    //  This resizes the game world to match the layer dimensions
    layer.resizeWorld();
	
 game.camera.focusOnXY(0, 0);
 

	ready = false;
}

function update()
{
	
	layer.x +=1;
	layer.y +=1;
   // map.layer.x = -game.camera.x;
   // map.layer.y = -game.camera.y;
	
    if (cursors.left.isDown)
    {
        tank.angle -= 4;
    }
    else if (cursors.right.isDown)
    {
        tank.angle += 4;
    }

    if (cursors.up.isDown)
    {
        //  The speed we'll travel at
        currentSpeed = 300;
    }
    else
    {
        if (currentSpeed > 0)
        {
            currentSpeed -= 4;
        }
    }

    if (currentSpeed > 0)
    {
        game.physics.arcade.velocityFromRotation(tank.rotation, currentSpeed, tank.body.velocity);
    }

}

